# Basic Sample Business Network

> This is the "Hello World" of Hyperledger Composer samples, which demonstrates the core functionality of Hyperledger Composer by changing the value of an asset.

This business network defines:

**Participant**
`REB`
`Researcher`
`Proof`

**Asset**
`Term`
`Consent`
`Proposal`

**Transaction**
`InsertTerm`
`DeleteTerm`
`InsertConsent`
`UpdateConsent`
`InsertProposal`
`UpdateProposal`

**Event**
`<TODO-EVENTS>`

